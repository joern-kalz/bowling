package bowling.calculator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

public class CalculatorTest {

    @Test
    public void shouldCorrectlyGroupRollsInFrames() {
        List<List<Integer>> frames = groupRollsInFrames(Arrays.asList(1, 4, 4, 5, 6, 4, 5, 5, 10, 0, 1, 7, 3, 6, 4, 
                10, 2, 8, 6));
        
        assertTrue(Calculator.isLastFrameFinished(frames));
        assertEquals(10, frames.size());
    }

    @Test
    public void shouldCalculateScore() {
        List<List<Integer>> frames = groupRollsInFrames(Arrays.asList(1, 4, 4, 5, 6, 4, 5, 5, 10, 0, 1, 7, 3, 6, 4, 
                10, 2, 8, 6));
        
        assertEquals(133, Calculator.calculateScore(frames));
    }
    
    private List<List<Integer>> groupRollsInFrames(List<Integer> rolls) {
        List<List<Integer>> frames = new ArrayList<>();
        
        for (Integer roll : rolls) {
            if (frames.size() == 0 || Calculator.isLastFrameFinished(frames)) {
                frames.add(new ArrayList<>());
            }
            
            frames.get(frames.size() - 1).add(roll);
        }
        
        return frames;
    }
}
