package bowling.calculator;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

/**
 * Bowling Scoring App
 */
public class App 
{
    
    public static void play() {
        try (Scanner scanner = new Scanner(System.in)) {
            List<List<Integer>> frames = new ArrayList<>();
            
            for (int i = 0; i < 10; i++) {
                List<Integer> currentFrame = new ArrayList<>();
                frames.add(currentFrame);
                
                do {
                    System.out.format("current score: %d%n", Calculator.calculateScore(frames));
                    System.out.format("Enter result of frame %d roll %d: ", frames.size(), currentFrame.size() + 1);
                    
                    currentFrame.add(scanner.nextInt());
                } while (!Calculator.isLastFrameFinished(frames));
            }

            System.out.format("final score: %d", Calculator.calculateScore(frames));
        } catch (InputMismatchException e) {
            System.out.println("invalid input");
        }
    }
    
    public static void main( String[] args )
    {
        App.play();
    }
}
