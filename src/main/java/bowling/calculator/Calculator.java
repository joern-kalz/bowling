package bowling.calculator;

import java.util.List;
import java.util.stream.Collectors;

public class Calculator {

    /**
     * Tests if the last frame in the list is finished
     * @param frames list of frames with each frame being a list of rolls
     * @return true if the frame is finished, false otherwise
     */
    public static boolean isLastFrameFinished(List<List<Integer>> frames) {
        List<Integer> rolls = frames.get(frames.size() - 1);
        int sumOfRolls = rolls.stream().mapToInt(it -> it).sum();
        
        if (frames.size() < 10) {
            return rolls.size() == 2 || (rolls.size() == 1 && sumOfRolls == 10);
        }
        
        return rolls.size() == 3 || (rolls.size() == 2 && sumOfRolls < 10);
    }
    
    /**
     * Calculates the total score of a finished or unfinished list of frames
     * @param frames list of frames with each frame being a list of rolls
     * @return total score
     */
    public static int calculateScore(List<List<Integer>> frames) {
        int score = 0;
        
        for (int i = 0; i < frames.size(); i++) {
            List<Integer> rolls = frames.get(i);
            int sumOfRolls = rolls.stream().mapToInt(it -> it).sum();
            score += sumOfRolls;
            
            if (sumOfRolls < 10 || i == 9) {
                continue;
            }
            
            List<List<Integer>> remainingFrames = frames.subList(i + 1, frames.size());
            List<Integer> remainingRolls = remainingFrames.stream().flatMap(List::stream).collect(Collectors.toList());
            
            if (rolls.get(0) < 10) {
                score += remainingRolls.size() > 0 ? remainingRolls.get(0) : 0;
            } else {
                List<Integer> nextTwoRolls = remainingRolls.subList(0, Math.min(2, remainingRolls.size()));
                int sumOfNextTwoRolls = nextTwoRolls.stream().mapToInt(it -> it).sum();
                score += sumOfNextTwoRolls;
            }
        }

        return score;
    }

}
